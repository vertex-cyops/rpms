# Packages

VertexXDR zip packages as resource for building rpms. The packages here are only for development.
Access are through https://gitlab.com/vertex-cyops/packages/-/raw/main/{folder-name}/{filename}

Example:
wget https://gitlab.com/vertex-cyops/packages/-/raw/main/ui/wazuh-4.5.0-1.zip 
